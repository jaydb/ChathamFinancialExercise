﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Normalizer.Tests
{
    [TestClass()]
    public class PercentNormalizerTests
    {
        [TestMethod()]
        // Testing number with a valid percent sign
        public void NormalizeTestWithPercent()
        {
            PercentNormalizer percentNormalizer = new PercentNormalizer();
            var result = percentNormalizer.Normalize("12%");
            Assert.AreEqual(result, "0.12");
        }

        [TestMethod()]
        //Test number with no percent sign
        public void NormalizeTestWithoutPercent()
        {
            PercentNormalizer percentNormalizer = new PercentNormalizer();
            var result = percentNormalizer.Normalize("12");
            Assert.IsTrue(result == "12");
        }

        [TestMethod()]
        //Testing formated number with vaild percent sign. Example format: X,XX,XXX.XX%
        public void NormalizeTestWithPercentAndComma()
        {
            PercentNormalizer percentNormalizer = new PercentNormalizer();
            var result = percentNormalizer.Normalize("12,76. 15%");
            Assert.AreEqual(result, "12.7615");
        }

        [TestMethod()]
        //Testing formated number without percent sign. Example format: X,XX,XXX.XX
        public void NormalizeTestCommaWithoutPercent()
        {
            PercentNormalizer percentNormalizer = new PercentNormalizer();
            var result = percentNormalizer.Normalize("12,76. 15");
            Assert.AreEqual(result, "1276.15");
        }

        [TestMethod()]
        //Testing negavite numbers without percent sign
        public void NormalizeTestNegativeNumberWithoutPercent()
        {
            PercentNormalizer percentNormalizer = new PercentNormalizer();
            var result = percentNormalizer.Normalize("-1");
            Assert.AreEqual(result, "-1");
            result = percentNormalizer.Normalize("-1.2");
            Assert.AreEqual(result, "-1.2");
        }

        [TestMethod()]
        ////Testing negative number with vaild percent sign.
        public void NormalizeTestNegativeNumberWithPercentComma()
        {
            PercentNormalizer percentNormalizer = new PercentNormalizer();
            var result = percentNormalizer.Normalize("-1%");
            Assert.AreEqual(result, "-0.01");
            result = percentNormalizer.Normalize("-1.254%");
            Assert.AreEqual(result, "-0.01254");
        }

        [TestMethod()]
        //Testing trailing decimal 
        public void NormalizeTestTrailingDecimal()
        {
            PercentNormalizer percentNormalizer = new PercentNormalizer();
            var result = percentNormalizer.Normalize("1696969%");
            Assert.AreEqual(result, "16969.69");
            result = percentNormalizer.Normalize("1.696969%");
            Assert.AreEqual(result, "0.01696969");
            result = percentNormalizer.Normalize("1.696969");
            Assert.AreEqual(result, "1.696969");
            result = percentNormalizer.Normalize("1696969");
            Assert.AreEqual(result, "1696969");
        }

        [TestMethod()]
        //Testing for presence of space, tabs and withspaces
        public void NormalizeTestWhiteSpaces()
        {
            PercentNormalizer percentNormalizer = new PercentNormalizer();
            var result = percentNormalizer.Normalize("\t    1     .3  5%");
            Assert.AreEqual(result, "0.0135");
            result = percentNormalizer.Normalize("  \t    1\t%");
            Assert.AreEqual(result, "0.01");
            result = percentNormalizer.Normalize("      \t      132\t");
            Assert.AreEqual(result, "132");
            result = percentNormalizer.Normalize("1 \t  .   \t2  5");
            Assert.AreEqual(result, "1.25");
        }

        [TestMethod()]
        //Testing for presence of invalid percent sign
        [ExpectedException(typeof(FormatException))]
        public void NormalizeTestInvalidPercentSign()
        {
            PercentNormalizer percentNormalizer = new PercentNormalizer();
            var result = percentNormalizer.Normalize("\t    %1     .3  5");
            result = percentNormalizer.Normalize("  \t    %1\t");
            result = percentNormalizer.Normalize("      \t      13%2\t");
            result = percentNormalizer.Normalize("1 \t  .   %\t2  5");
        }

        [TestMethod()]
        //Testing for null/empty or whitespace exception
        [ExpectedException(typeof(ArgumentNullException))]
        public void NormalizeTestNullInput()
        {
            PercentNormalizer percentNormalizer = new PercentNormalizer();
            var result = percentNormalizer.Normalize(" ");
            result = percentNormalizer.Normalize("");
        }

        [TestMethod()]
        //Testing for invalid number eg: alphanumeric
        [ExpectedException(typeof(FormatException))]
        public void NormalizeTestAlphaNumeric()
        {
            PercentNormalizer percentNormalizer = new PercentNormalizer();
            var result = percentNormalizer.Normalize("12aa5");
        }
    }
}