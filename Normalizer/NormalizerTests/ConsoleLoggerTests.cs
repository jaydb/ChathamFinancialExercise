﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace Normalizer.Tests
{
    [TestClass()]
    public class ConsoleLoggerTests
    {
        [TestMethod()]
        //Testing for ILogger(String) execution with string
        public void LogTestString()
        {
            string result = "1287";
            Mock<ILogger> clog = new Mock<ILogger>();
            clog.Object.Log(result);
            clog.Verify(mock => mock.Log(result), Times.Once);
        }

        [TestMethod()]
        //Testing for Ilogger(Exception) execution with FormatException
        public void LogTestFormatException()
        {
            Exception ex = new FormatException("Format exception");
            Mock<ILogger> clog = new Mock<ILogger>();
            clog.Object.Log(ex);
            clog.Verify(mock => mock.Log(ex), Times.Once);
        }

        [TestMethod()]
        //Testing for Ilogger(Exception) execution with ArgumentNullException
        public void LogTestNullException()
        {

            Exception ex = new ArgumentNullException("Null exception");
            Mock<ILogger> clog = new Mock<ILogger>();
            clog.Object.Log(ex);
            clog.Verify(mock => mock.Log(ex), Times.Once);
        }
    }
}